# Linux (Debian/Ubuntu)

#sudo apt-get install git curl php php-xml zip

#Java
if [ ! -d /opt/jdk1.8.0_211/ ]
then
	if [ -f jdk-8u211-linux-x64.tar.gz ]
	then
	echo "See https://www.oracle.com/technetwork/java/javase/terms/license/index.html"
	echo "Installing Oracle JDK 1.8.0.211 with JRE to /opt/jdk1.8.0_211/jre"
	sudo tar -C /opt -zxvf jdk-8u211-linux-x64.tar.gz
	fi
else

	JRE_HOME=/opt/jdk1.8.0_211/jre/
	export JRE_HOME

	JAVA_HOME=/opt/jdk1.8.0_211/
	export JAVA_HOME

	echo $JAVA_HOME
	echo $JRE_HOME

fi


cd /opt

git clone --depth 1 https://bitbucket.org/wkennedy/teams-in-space-demo atlassian


. /opt/atlassian/setup.sh


tis-install && tis-start &

sh openem.sh
