# Teams in Space Demo

This is a fork of Atlassian's TIS repo.

It includes a script to query the marketplace API for current product versions. Running it requires jq.

This is the output.

Crowd
"3.4.5"
JIRA
"8.2.1"
Confluence
"6.15.4"
Bitbucket
"6.3.2"
Bamboo
"6.9.1"
Fisheye
"4.7.0"
Crucible
"4.7.0"

Alter versions in scripts/products.xml to reflect the target version.

The versions above are referenced and will be installed.


## What is Teams in Space?

Teams in Space is a fully featured demo of
JIRA, Confluence, Bitbucket, Bamboo, FeCru, and Crowd.
Teams in Space is a fictional company
specializing in space travel for teams of over 6 people.
Want to learn more?
Meet Teams in Space!!!

## What's new?

2019-02-15 - Teams in Space includes the following application releases:
JIRA 8.0.0,
JIRA Service Desk 4.0.0,
Confluence 6.13.0,
Bitbucket Server 5.16.0,
Bamboo 6.6.1,
Crowd 3.2.3,
FeCru 4.6.0,
& Portfolio 2.9

## Known issues/features

* TIS is intended to run on a single instance and be accessed locally - change hostnames and context paths at your own peril (for now)
* Something causes the git remote to get screwed up. It seems that Bamboo does something weird that affects the top level remotes. To fix it (assuming you have an ssh key set up):

        cd /opt/atlassian
        git remote set-url origin git@bitbucket.org:atlassian/teams-in-space-demo.git

* Bamboo needs more memory than the default (or, more likely, is attempting to run more builds than it should)
* Some reports/features are sensitive to timezone settings and/or the passage of time
* If you are using ZSH and have trouble using the tis- commands, try using bash

## Install the prerequisites

If you are upgrading from a previous instance, it is strongly recommended that you start over:

        cd /opt
        mv atlassian atlassian.old

### Mac

1.  Acquire a Mac system with minimum 8G of ram.  16G is recommended.  The new macbook pro with 32G is even more preferable. Windows is not supported at this time.  Linux works as well but will require a different java setup as well as PHP 5.4.
2.  Install [git](http://git-scm.com/download/mac). If you get an "unsigned binary error", right click the package and choose open.  MacOS will ask you to confirm installation.
3.  Install a java [jdk](http://www.oracle.com/technetwork/java/javase/downloads/index.html) Use the installer from Oracle. Make sure it's version 1.8 (Java 8).
4.  Set JAVA_HOME and configuration variables. Paste the export command inside of the nano window.

    nano .bash_profile

    export JAVA_HOME=$(/usr/libexec/java_home)
    source /opt/atlassian/setup.sh

You can close nano by using control-x and choosing save.

### Linux (Debian/Ubuntu)
1. Install git, curl, PHP, zip

    apt-get install git curl php php-xml zip

2. Install Oracle JAVA. You will need to add an apt repository, for which you may need a couple of apt packages to make it easy.

    apt-get install software-properties-common apt-utils
    add-apt-repository ppa:webupd8team/java
    apt-get update;apt-get install oracle-java8-installer oracle-java8-set-default

3. Set JAVA_HOME:

    echo "export JAVA_HOME=\"/usr/lib/jvm/java-8-oracle\"" >>~/.bashrc; export JAVA_HOME="/usr/lib/jvm/java-8-oracle"
    echo "source /opt/atlassian/setup.sh">>~/.bashrc


## Install Teams in Space

4. Run the install-tis.sh script

This script will perform the necessary installation tasks.

Note: *You must edit and set the JRE_HOME and JAVA_HOME variables for your setup.*

You may see a couple of PHP warnings about directories already existing or an "invalid argument for foreach()". This is usually ok and can be ignored.

5. Scan through the console output for any unusual output (especially downloads timing out). If it appears ok, go ahead and start the instance:

        tis-reset

You will be prompted to press "y" to continue.

The tis-reset command will attempt to stop, reset, move dates to present, start, reindex and smoketest the applications all in one go. It usually takes a few minutes

7. Check the smoketest output. A series of URLs are checked to make sure they're returning expected strings. It's possible that an application does not start properly, especially in low memory situations.

## Where are my apps?

When the applications have been started, here is how to access them from your web browser.  Click the application name to go to the demo guide.  For global settings like logins, go to the [Teams in Space Guide](https://partners.atlassian.com/display/resources/Teams+in+Space+Demo+Guides)

| Application | URL | Username | Password | Notes |
|-------------|-----|----------|----------|-------|
| [Bamboo](https://experts.atlassian.com/display/resources/Bamboo+Demo+Guide) | <http://bamboo.teamsinspace.com:8085> | admin | Charlie! |   |
| [Confluence](https://experts.atlassian.com/display/resources/Confluence+Demo+Guide) | <http://confluence.teamsinspace.com:8090> | admin | Charlie! |Testing JIRA Service Desk KB integration, ensure that log in to Confluence as the same user you are logged into JIRA as, otherwise creating articles from Service Desk will fail. |
| [Crowd](https://experts.atlassian.com/display/resources/Crowd+Demo+Guide) | <http://crowd.teamsinspace.com:2430/crowd> | admin | Charlie! |   |
| [Fisheye/Crucible](https://experts.atlassian.com/display/resources/FeCru+Demo+Guide) | <http://fecru.teamsinspace.com:8060> | admin | Charlie! |   |
| [JIRA](https://experts.atlassian.com/display/resources/JIRA+Demo+Guide) | <http://jira.teamsinspace.com:8080> | admin | Charlie!  | Enable/Disable "JIRA Remote Link Aggregator Plugin" after startup. If needing to demonstrate JIRA Service Desk KB integration, than must start JIRA with <http://127.0.0.1:8080> or computer name (just not localhost) to ensure JIRA and Confluence stop squashing each others cookies  |
| [Service Desk](https://experts.atlassian.com/display/resources/Service+Desk+Demo+Guide) | <http://jira.teamsinspace.com:8080>  | admin | Charlie!  | Enable/Disable "JIRA Remote Link Aggregator Plugin" after startup |
| [Bitbucket](https://experts.atlassian.com/display/resources/Bitbucket+Demo+Guide) | <http://bitbucket.teamsinspace.com:7990> | admin | Charlie! |  |

## Running Teams in Space remotely

Not everyone wants to run TIS on their local machine as they don't have a MacBook Pro or want to demo two or more users working with TIS at the same time.  To support these types of setups, you'll need to port forward from your machine to the machine running TIS.

You'll need to create a shared account on the TIS server machine so that your users can login via ssh.  If you're not familiar with how to do this, ping someone in IT to help you.  To test if the shared account is working, you can run the following command in the Mac Terminal:

        ssh ${REMOTEMACHINE} -l ${REMOTEUSER}

For example, `ssh tismacsersver -l demoaccount` or `ssh ec2-user@198.123.456.789`

Once that works, you can then forward all network traffic over to the remote machine using the following command:

        ssh -L 8085:127.0.0.1:8085 -L 8090:127.0.0.1:8090 -L 2430:127.0.0.1:2430 -L 8080:127.0.0.1:8080 -L 7990:127.0.0.1:7990 user@remotemachine

This command should be executed on your local machine which will forward all the network traffic to your remote machine.  The process will automatically log you into the remote machine as specified by the remote address.

## Tips to make life easier

* Shut down everything you can on your Mac if you have only 8G of ram.  Our apps LOVE memory.
* If you're using a VM such as EC2 on AWS, we recommend using an Instance Type of t2.large or t2.xlarge.
* Click remember me when logging in.  I'm troubleshooting an SSO issue with Crowd that's not remembering login w/o the remember me box checked.
* Sometimes Bamboo branches in JIRA appear as "not found". Refreshing the page makes it go away.
* Ensure you index the apps.  See step 8 above.
* You can stop apps by using the command stop-app (e.g. stop-crowd)

## Available commands

In addition to the tis-install and tis-reset commands above, some subcommands are available if you require finer grained control.

**Warning: Using anything other than tis-reset to undo changes will not guarantee proper cleanup of caches and other files**

* tis-install (install all products)
* tis-start (start all products)
* tis-stop (stop all products)
* tis-check (list running processes)
* tis-commit (stop processes, run a git status, doesn't actually commit anything currently)
* tis-reindex (reindex conf, jira, bam)
* tis-clean (reset the working tree, and pull in any new changes)
* tis-smoketest (curl some urls defined in smoketest_data.txt and grep for an expected string)
* tis-reset (stop, clean, start, reindex, smoketest)
* tis-reset --hard (stop, clean --hard, install, start, reindex, smoketest)
* tis-fixfecruapplinks fixes wrong OAuth on existing FECru applinks, removes orphaed trusted applinks in FECru

* scripts/dns-override.sh can be used to override the A records for *.teamsinspace.com in /etc/hosts. This may come handy if the local TIS demo is broken and there's another one available on the local network. Use with the -r switch to revert the changes.

## Resetting the instance

* If you wish to blow away data changes, simply type tis-reset. This will stop services, reset the git repository, start services, reindex and run a smoke test.
* If you wish to test the entire software install and setup process (eg after updating a product version), type tis-reset --hard. This will do the same as above but also blows away any non-tracked files including software, and reinstalls everything.

## Smoke tests

A series of basic smoke tests can be run to verify that a TIS instance is generally available at any time. To run it, first start tis (tis-reset), then run tis-smoketest. If any test should happen to fail, check the URL manually using curl, or a web browser. Tests can be added by editing smoketest\_data.txt, verifying the result and committing the change. Some common causes of failures:

* Product failed to install - check the inst dir
* Plugin failed to install - check the corresponding plugin dir
* Reindex needed - run tis-reindex
* License expired - check the app manually
* Still starting up - Bitbucket will respond to requests before it's finished starting up. Wait.
* Data or product changed - check the URL and corresponding string test manually

## Troubleshooting

Please check the following:

* You have 16Gb or more of memory
* You have git and Java 8
* You have cloned the right repository (cd /opt/atlassian; git remote -v)
* Repository is based at the right directory (there should be an /opt/atlassian/scripts directory)
* The master branch is checked out (confirm by checking the first line of output from a git status command)
* You have the latest commits (compare git log with the repo). If not, git pull until you do
* Get the latest tis-\* commands into your shell. Running source setup.sh should echo "Teams in Space is set up!"
* Do a hard reset of the instance. tis-reset --clean will remove all tracked and untracked files, reinstall the software and fire everything up.
  * If the reset says that the processes haven't all stopped, try it again and/or kill any remaining TIS related processes (tis-check will list them).
  * tis-reset will ask for confirmation before continuing (this is a safety in case you have uncommitted changes that you want to keep). Press "y" to continue.
* After starting the products, tis-reset runs a reindex and a smoketest. If the smoketest shows any failures check the affected product (it might still be starting up). If the smoketest returns many failures, it might just be that crowd needs restarting. Try "stop-crowd", check that it stopped, then "start-crowd". Restarting other products may or may not be necessary as well.
* If a product doesn't start at all, the install may not have completed successfully.
  * Check that it exists in /opt/atlassian/inst/\<product>
  * If it exists, check the logs in /opt/atlassian/data/\<product>/... (a command line alias less-\<product>-log may be defined as a shortcut)
* If a plugin is missing, the install may not have completed successfully.
  * Check that it exists in the appropriate plugin directory
  * Check in UPM that it's installed, enabled and licensed
  * Check logs using "tail-jira-log" or "less-jira-log" for any of the applications.
  * Try using "check-jira" to check if just the Jira service is running.  This can be done for any of the applications using "check-*"

Still broken? Please raise an issue.

## Contributing changes

The following changes are welcome via a pull request, assuming they will generally benefit all users of Teams in Space:

* Changes to scripts/products.xml to bump to new production versions of software and/or plugins
* Improvements to the scripts in setup.sh or the scripts/ directory
* Extra smoke tests in smoketest\_data.txt

For license updates or data changes (basically anything that gets stored in the data/ directory), please raise an issue (TODO: where?) so we can provide guidance on doing this. Data changes can't be merged with a traditional git workflow, so we need to manage these updates carefully. It's also important to preserve the integrity of other demo data and not make a mess.
If in doubt, please ask.

### Updating product versions

There is a manifest file in:

        scripts/products.xml

Our products follow a consistent naming structure. You can update the version value for an individual product by editing the version node in an individual product.  For example to update Crowd,

    <products>
      <product>
        <shortname>Crowd</shortname>
        <location>inst/crowd</location>
        <downloadurl>http://downloads.atlassian.com/software/crowd/downloads/atlassian-crowd-^version^.tar.gz</downloadurl>
            <version>2.7.2</version>
            <filename>atlassian-crowd-^version^.tar.gz</filename>
            <dirname>atlassian-crowd-^version^</dirname>
          </product>

Update the version node only.  The install script will configure the download URL and filenames appropriatley.

**Always TEST the integration when updating versions!  Sometimes things do not integrate smoothly. Never check in configuration data from a prerelease product.  It will break those of us running the shipping version.**


## Editing Dates (for JIRA)

Scripts are provided to alter the dates of JIRA Teams in Space data. To update dates to the current time:

1. Ensure the file at /opt/atlassian/data/jira/database/jiradb-permanent.script has a comment on the top line containing a date, e.g. "-- 2014-02-03". This specifies the date in the database you want to be the 'current date'

        nano /opt/atlassian/data/jira/database/jiradb-permanent.script

2. Change to the scripts directory

        cd /opt/atlassian/scripts

3. Run the movetopresent script. This will alter the dates in jiradb-permanent.script and save it to jiradb.script (which JIRA reads when it starts)

        ./movetopresent.sh

4. Start JIRA as usual (remember to re-index)

        start-jira

Once you are done playing around with the instance (adding/editing tickets etc.), you can save the changes for next time by:

1. Stop JIRA gracefully

        stop-jira

2. Change to the scripts directory

        cd /opt/atlassian/scripts

3. Run the savetopermanent script - this will save the snapshot of your instance to jiradb-permanent.script for later use

        ./savetopermanent.sh

4. To update your dates to current next time, follow the update instructions above to run movetopresent.sh

  [git]: http://git-scm.com/download/mac
  [java]: http://java.com/en/
  [Meet the SeeSpaceEZ Team]: http://confluence.teamsinspace.com:8090/display/TIS/Meet+the+SeeSpaceEZ+Team
  [tisadmin@veryrealemail.com]: http://mailinator.com/inbox.jsp?to=tisadmin
  [agrant@veryrealemail.com]: http://mailinator.com/inbox.jsp?to=agrant
  [cowens@veryrealemail.com]: http://mailinator.com/inbox.jsp?to=cowens
  [wsmith@veryrealemail.com]: http://mailinator.com/inbox.jsp?to=wsmith
  [jevans@veryrealemail.com]: http://mailinator.com/inbox.jsp?to=jevans
  [mtaylor@veryrealemail.com]: http://mailinator.com/inbox.jsp?to=mtaylor
  [kcampbell@veryrealemail.com]: http://mailinator.com/inbox.jsp?to=kcampbell
  [eparis@veryrealemail.com]: http://mailinator.com/inbox.jsp?to=eparis
  [summitrlee@gmail.com]: mailto:summitrlee@gmail.com
  [hjennings@veryrealemail.com]: http://mailinator.com/inbox.jsp?to=hjennings
  [mdavis@veryrealemail.com]: http://mailinator.com/inbox.jsp?to=mdavis
  [@veryrealemail.com]: mailto:blah@veryrealemail.com
  [teamsinspace@gmail.com]: mailto:teamsinspace@gmail.com
  [summitjira@gmail.com]: mailto:summitjira@gmail.com
  [james+tis@atlassian.com]: mailto:james+tis@atlassian.com
  [1]: mailto:agrant@veryrealemail.com

## Connect to Jira H2 Database

If you'd like to access the database for Jira the same way you access a Postgres or MySQL database, you can do this using the h2 embedded database connector. MAKE SURE JIRA IS STOPPED BEFORE YOU TRY TO CONNECT TO THE DATABASE.

1. Run the following command in terminal

        open /opt/atlassian/lib/h2-1.4.185.jar

2. You will see a new browser tab open with the H2 Embedded connection information. Make sure the following are listed in these fields
        
        Driver Class: org.h2.Driver
        JDBC URL: jdbc:h2:tcp://localhost:9092//opt/atlassian/data/jira/database/h2db
        User Name: sa
        Password should be blank

3. Click "Test Connection" and wait for the red text to say "Test successful".

4. Click "Connect" and you will then have access to the database.
