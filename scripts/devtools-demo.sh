WEBSITE_CLONE_BASE=/Users/rbarnes/website

function pressany() {
    read -p "${1:-"Do you want to proceed with this step?"} " -n 1 -r
    echo
    if [[ $REPLY =~ ^[Yy]$ ]]; then
        return 0
    else
        return 1
    fi
}

function dologin() {
    echo "Ok! Let's get things set up! First log in to all the apps as eparis. Tick \"Rememeber login\" on each"
    if pressany; then
        open http://jira.teamsinspace.com:8080/
        open http://stash.teamsinspace.com:7990/
        open -a SourceTree
    fi
}

function start-website() {
    echo "Next, let's start up the \"production\" web server..."
    if pressany; then
        CWD=$(pwd)
        cd /opt/atlassian/data/bitbucket/tmp/
        if [ ! -d "website" ]; then
            git clone http://eparis@stash.teamsinspace.com:7990/scm/tis/website.git website
        fi
        cd website
        git pull
        python -m SimpleHTTPServer 1234 &
        open "http://localhost:1234/Teams%20In%20Space.html"
        cd $CWD
    fi
}

function stop-website() {
    echo "Stopping website..."
    pkill -f "SimpleHTTPServer 1234"
}

function start-flight() {
    echo "Next, let's start up the \"production\" web server..."
    if pressany; then
        CWD=$(pwd)
        cd /opt/atlassian/data/bitbucket/tmp/
        if [ ! -d "flight" ]; then
            git clone http://eparis@bitbucket.teamsinspace.com:7990/scm/fb/flight-bites-web-app.git flight
        fi
        cd flight
        git pull
        npm install
        open "http://localhost:9005" && grunt 
        cd $CWD
    fi
}

function create-demo-issue() {
    echo "Creating demo issue..."
    if pressany; then
        # Create an issue - http://jira.teamsinspace.com:8080/rest/api/2/issue/createmeta
        read -r -d '' issuedata <<-'EOF'
        {
            "fields": {
               "project":
               {
                  "key": "TIS"
               },
               "summary": "Martian Privacy Directive compliance",
               "fixVersions": [{"id":"10100"}],
               "description": "The Martian Privacy Directive requires that we show an announcement on the web site to declare the changes. We already have privacy.html, just need to link to it in a banner.",
               "issuetype": {
                  "name": "Bug"
               }
           }
        }
EOF
        issueresponse=$(curl -u rlee:Charlie! -X POST --data "$issuedata" -H "Content-Type: application/json" http://jira.teamsinspace.com:8080/rest/api/2/issue/)
        echo $issueresponse
        issueid=$(echo $issueresponse | python -c 'import sys, json; print json.load(sys.stdin)[sys.argv[1]]' id)
        issuekey=$(echo $issueresponse | python -c 'import sys, json; print json.load(sys.stdin)[sys.argv[1]]' key)
        echo "Got $issuekey (id: $issueid)"

        echo "Assigning..."
        assigndata="{\"fields\": {\"assignee\":{\"name\":\"eparis\"}}}"
        assignresponse=$(curl -u rlee:Charlie! -X PUT --data "$assigndata" -H "Content-Type: application/json" http://jira.teamsinspace.com:8080/rest/api/2/issue/$issuekey)
        echo $assignresponse

        sprintdata="{\"idOrKeys\":[\"$issuekey\"],\"customFieldId\":10004,\"sprintId\":1,\"addToBacklog\":false}"
        sprintresponse=$(curl -u rlee:Charlie! -X PUT --data "$sprintdata" -H "Content-Type: application/json" http://jira.teamsinspace.com:8080/rest/greenhopper/1.0/sprint/rank)
        echo $sprintresponse
    fi
}

function open-rapid-board() {
    echo "Want me to open the rapid board?"
    if pressany; then
        open "http://jira.teamsinspace.com:8080/secure/RapidBoard.jspa?rapidView=3&view=detail"
    fi
}

function make-some-changes() {
    if pressany "Shall I make some changes for you to commit?"; then
        (cd $WEBSITE_CLONE_BASE; git cherry-pick -n demo1)
    fi
}

function make-some-comments() {
    if pressany "Shall I make some comments on your pull request?"; then
    read -r -d '' commentdata <<-'EOF'
    {
        "text": "This piggy?",
        "anchor": {
            "line": 157,
            "lineType": "ADDED",
            "path": "Teams In Space.html"
        }
    }
EOF
        curl -u rlee:Charlie! 'http://stash.teamsinspace.com:7990/rest/api/latest/projects/TIS/repos/website/pull-requests/2/comments' -H 'Content-Type: application/json' -H 'Accept: application/json, text/javascript, */*; q=0.01' --data-binary "$commentdata"
    fi
}

function make-some-more-changes() {
    if pressany "Shall I make some changes for you to commit?"; then
        (cd $WEBSITE_CLONE_BASE; git cherry-pick -n demo2)
    fi
}

function show-release-report() {
    if pressany "Let's look at the release report?"; then
        open "http://jira.teamsinspace.com:8080/browse/TIS/fixforversion/10100/?selectedTab=com.atlassian.jira.plugins.jira-development-integration-plugin:release-report-tabpanel"
    fi
}

function tis-devtools-demo() {
    dologin
    start-website
    create-demo-issue
    open-rapid-board
    pressany "Now create a branch, and check it out in sourcetree."
    make-some-changes
    make-some-comments
    make-some-more-changes
    show-release-report
    echo "Demo done!"
    stop-website
}

