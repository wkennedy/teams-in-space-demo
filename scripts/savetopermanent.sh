#!/bin/sh
echo "\n"
echo "Saving JIRA Data"
DB_PATH_JIRA=/opt/atlassian/data/jira/database
H2_JAR=/opt/atlassian/lib/h2-1.4.185.jar
# export data from h2 db
java -cp $H2_JAR org.h2.tools.Script -script $DB_PATH_JIRA/jiradb.script -user sa -url "jdbc:h2:file:$DB_PATH_JIRA/h2db"
# move to permanent
php /opt/atlassian/scripts/migrate_dates.php saveDatesPermanent $DB_PATH_JIRA/jiradb.script $DB_PATH_JIRA/jiradb-permanent.script
# remove jiradb.script
rm -f $DB_PATH_JIRA/jiradb.script

# echo "\n"
# echo "Saving Confluence Data"
# php migrate_dates.php saveDatesPermanent ../data/confluence/database/confluencedb.script ../data/confluence/database/confluencedb-permanent.script

echo "\n"
echo "Saving Confluence Data"
DB_PATH_CONFLUENCE=/opt/atlassian/data/confluence/database
H2_JAR=/opt/atlassian/lib/h2-1.4.185.jar
# export data from h2 db
java -cp $H2_JAR org.h2.tools.Script -script $DB_PATH_CONFLUENCE/confluencedb.script -user sa -url "jdbc:h2:file:$DB_PATH_CONFLUENCE/h2db" -password ""
# move to permanent
php /opt/atlassian/scripts/migrate_dates.php saveDatesPermanent $DB_PATH_CONFLUENCE/confluencedb.script $DB_PATH_CONFLUENCE/confluencedb-permanent.script
# remove confluencedb.script
rm -f $DB_PATH_CONFLUENCE/confluencedb.script