#!/bin/sh
echo "\n"
echo "Migrating JIRA Data"
DB_PATH_JIRA=/opt/atlassian/data/jira/database
H2_JAR=/opt/atlassian/lib/h2-1.4.185.jar
# shift dates
php /opt/atlassian/scripts/migrate_dates.php shiftDatesToPresent $DB_PATH_JIRA/jiradb-permanent.script $DB_PATH_JIRA/jiradb.script
# delete h2 database
rm -f $DB_PATH_JIRA/h2db.mv.db
rm -f $DB_PATH_JIRA/h2db.trace.db
# create h2 database from jiradb.script
java -cp $H2_JAR org.h2.tools.RunScript -script $DB_PATH_JIRA/jiradb.script -url "jdbc:h2:file:$DB_PATH_JIRA/h2db"
# remove jiradb.script
rm $DB_PATH_JIRA/jiradb.script


# echo "\n"
# echo "Migrating Confluence Data"
# php migrate_dates.php shiftDatesToPresent ../data/confluence/database/confluencedb-permanent.script ../data/confluence/database/confluencedb.script



echo "\n"
echo "*** Confluence Data Migration disabled currently ***"
DB_PATH_CONFLUENCE=/opt/atlassian/data/confluence/database
H2_JAR=/opt/atlassian/lib/h2-1.4.185.jar
# shift dates 
# php migrate_dates.php shiftDatesToPresent $DB_PATH_CONFLUENCE/confluencedb-permanent.script $DB_PATH_CONFLUENCE/confluencedb.script
# delete h2 database
# rm -f $DB_PATH_CONFLUENCE/h2db.h2.db
# rm -f $DB_PATH_CONFLUENCE/h2db.trace.db
# create h2 database from confluencedb.script
# java -cp $H2_JAR org.h2.tools.RunScript -script $DB_PATH_CONFLUENCE/confluencedb.script -url "jdbc:h2:file:$DB_PATH_CONFLUENCE/h2db;MV_STORE=FALSE;MVCC=FALSE" -user 'sa' -password ''
# remove confluencedb.script
# rm $DB_PATH_CONFLUENCE/confluencedb.script