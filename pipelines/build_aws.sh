#!/bin/bash

apt-get update -y
apt-get install -y zip unzip

export PACKER_VERSION="0.9.0" #SHould remove this and set it in pipelines environment vars
git clone davidglennjenkins@bitbucket.org:davidglennjenkins/packer-aws.git

echo "===> Installing Packer and cleanup..."                                                                 && \
rm -rf /usr/sbin/packer                                                                                      && \
curl -LO https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip    && \
unzip packer_${PACKER_VERSION}_linux_amd64.zip -d /usr/bin/                                                  && \
rm -rf packer_${PACKER_VERSION}_linux_amd64.zip  															 && \
cd ${BITBUCKET_CLONE_DIR}																					 && \
touch ami.txt																								 && \
ls -al																										 && \
pwd																											 && \
export AWS_ACCESS_KEY_ID=$AWS_ACCESS																		 && \
export AWS_SECRET_ACCESS_KEY=$AWS_SECRET																	 && \
export AWS_DEFAULT_REGION=us-east-1																			 && \
cd /opt/atlassian/pipelines/agent/build/packer-aws															 && \
ls -al																										 && \
pwd																											 && \
packer build -machine-readable packer.json | tee build.log							 						 && \
grep 'artifact,0,id' build.log | cut -d, -f6 | cut -d: -f2 > ${BITBUCKET_CLONE_DIR}/ami.txt					 && \
cat ${BITBUCKET_CLONE_DIR}/ami.txt																			 && \
cd ${BITBUCKET_CLONE_DIR}																					 && \
amiID=`(cat ami.txt)`																						 && \
echo $amiID